package primitives;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.MLvalue;

public class Print implements Primitive {

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		MLvalue arg = mz.accu.clone();
		System.out.println(arg);
	}
	
	@Override
	public String toString() {
		return "print";
	}

}
