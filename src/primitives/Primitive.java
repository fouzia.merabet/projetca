package primitives;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;

public interface Primitive {

	public void apply(MiniZAM mz) throws InstructionEvaluationException;

}
