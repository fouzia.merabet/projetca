package primitives;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.Int;
import types.MLvalue;

public class Eq implements Primitive {

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		Int arg1 = (Int) mz.accu.clone();
		Int arg2 = (Int) ((MLvalue) mz.stack.pop()).clone();
		mz.accu = new Int((arg1.getValue() == arg2.getValue()) ? 1 : 0);
	}
	
	@Override
	public String toString() {
		return "=";
	}

}
