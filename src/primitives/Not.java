package primitives;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.Int;

public class Not implements Primitive {

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		Int arg = (Int) mz.accu.clone();
		mz.accu = new Int(~arg.getValue());
	}
	
	@Override
	public String toString() {
		return "not";
	}

}
