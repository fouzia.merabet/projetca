package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class ENVACC implements Instruction {

	private int index;

	public ENVACC(int index) {
		this.index = index;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			mz.accu = ((MLvalue) mz.env.get(index)).clone();
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "ENVACC " + this.index;
	}

}
