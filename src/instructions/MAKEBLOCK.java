package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.*;

public class MAKEBLOCK implements Instruction {
	
	private int n;

	public MAKEBLOCK(int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}


	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			// création du nouveau bloc 
			bloc b=new bloc();
			
			if (this.n > 0) {
				//la valeur présente dans accu constitue le premier elt du bloc
				b.add(mz.accu.clone());
				// n_a elements sont dépilés de stack
				for (int i = 0; i < this.n-1; i++)
					b.add(mz.stack.pop().clone());
				//l'accu contiendra le nouveau bloc crée
				mz.accu=b.clone();
			}
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

	}
	@Override
	public String toString() {
		return "MAKEBLOCK " + this.n;
	}

}
