package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class APPTERM implements Instruction {

	private int n;
	private int m;

	public APPTERM(int n, int m) {
		this.n = n;
		this.m = m;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {

			// Depilement des n arguments de l'appel
			Stack stack = new Stack();
			for (int i = 0; i < this.n; i++)
				stack.push(mz.stack.pop().clone());

			// Depilement des m-n variables locales de la fonction appelante
			for (int i = 0; i < this.m - this.n; i++)
				mz.stack.pop();

			// Rempilement des n arguments de l’appel
			for (int i = 0; i < this.n; i++)
				mz.stack.push(stack.pop().clone());

			// Positionnement dans le contexte de la fonction appelee
			Closure cls = (Closure) mz.accu;
			mz.pc = mz.address.get(cls.getLabel());
			mz.env = (Env) cls.getEnv().clone();

			// Incrementation de extra args de n-1
			mz.extra_args += (this.n - 1);

		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

	}

	@Override
	public String toString() {
		return "APPTERM " + this.n + "," + this.m;
	}

}
