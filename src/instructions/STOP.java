package instructions;

import miniZAM.MiniZAM;

public class STOP implements Instruction {

	@Override
	public void apply(MiniZAM mz) {
		mz.works = false;
	}

	@Override
	public String toString() {
		return "STOP";
	}

}
