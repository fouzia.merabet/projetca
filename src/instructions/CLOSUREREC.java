package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class CLOSUREREC implements Instruction {

	private String label;
	private int n;

	public CLOSUREREC(String label, int n) {
		this.label = label;
		this.n = n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {

		try {

			// si n > 0 alors l'accumulateur est empile
			if (this.n > 0)
				mz.stack.push(mz.accu.clone());

			// une fermeture dont le code correspond au label L et dont l'environnement est
			// constitue de n valeurs depilees de stack et creee et mise dans l'accumulateur
			Env env = new Env();
			for (int i = 0; i < this.n; i++)
				env.add(((MLvalue) mz.stack.pop()).clone());

			env.add(0, new Int(mz.address.get(this.label)));

			mz.accu = new Closure(label, env);

			mz.stack.push(mz.accu.clone());

			// incrementer le pc
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

	}

	@Override
	public String toString() {
		return "CLOSUREREC " + this.label + "," + this.n;
	}

}
