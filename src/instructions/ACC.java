package instructions;

import miniZAM.*;
import tools.*;

public class ACC implements Instruction {

	private int index;

	public ACC(int index) {
		this.index = index;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			mz.accu = mz.stack.get(index).clone();
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "ACC " + this.index;
	}

}
