package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.*;

public class GETVECTITEM implements Instruction {
	public GETVECTITEM() {};

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		
		try {
			// dépiler un element n de stack
			Int n= (Int) mz.stack.pop().clone();
			//mettre dans accu la n-iéme valeur du bloc dans l'accu
			mz.accu= ((bloc)mz.accu).get(n.getValue());
					
			} catch (Exception e) {
				throw new InstructionEvaluationException(this.toString());
			}

		mz.pc++;

	}
	@Override
	public String toString() {
		return "GETVECTITEM ";
	}
}
