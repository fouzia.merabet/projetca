package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.Int;

public class PUSHTRAPL implements Instruction {

	private String label;

	public PUSHTRAPL(String label) {
		this.label = label;
	}
	
	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			//empiler ...
			mz.stack.push(new Int(mz.extra_args));
			mz.stack.push(mz.env);
			mz.stack.push(mz.trap_sp);
			mz.stack.push(new Int(mz.address.get(this.label)));
			// met dans trap_sp un pointeur vers l'élément en tête de pile
			mz.trap_sp=(Int) mz.stack.get(0);
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
		mz.pc++;
	}
	public String toString() {
		return "PUSHTRAPL " + this.label;
	}


}
