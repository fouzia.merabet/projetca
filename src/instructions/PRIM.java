package instructions;

import miniZAM.*;
import primitives.*;
import tools.*;

public class PRIM implements Instruction {

	private Primitive operation;
	private String name;

	public PRIM(String op) throws Exception {
		switch (op) {
		case "+":
			this.operation = new Add();
			break;
		case "-":
			this.operation = new Sub();
			break;
		case "*":
			this.operation = new Times();
			break;
		case "/":
			this.operation = new Div();
			break;
		case "and":
			this.operation = new And();
			break;
		case "or":
			this.operation = new Or();
			break;
		case "not":
			this.operation = new Not();
			break;
		case "<>":
			this.operation = new Dif();
			break;
		case "=":
			this.operation = new Eq();
			break;
		case "<":
			this.operation = new Inf();
			break;
		case "<=":
			this.operation = new InfEq();
			break;
		case ">":
			this.operation = new Sup();
			break;
		case ">=":
			this.operation = new SupEq();
			break;
		case "print":
			this.operation = new Print();
			break;
		default:
			throw new Exception();
		}
		this.name = op;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			this.operation.apply(mz);
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "PRIM " + this.name;
	}

}