package instructions;

import miniZAM.MiniZAM;

import tools.InstructionEvaluationException;
import tools.Stack;

import types.*;

public class ASSIGN implements Instruction {
	private int n;

	public ASSIGN(int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			//remplacer la n-ième elt à partir du sommet de la pile par la valeur situé dans accu
			
			// n arguments sont depiles
						Stack stack = new Stack();
						for (int i = 0; i < this.n; i++)
							stack.push(mz.stack.pop());
			//supprimer la n-iéme valeur
						mz.stack.pop();
			// ajouter la valeur situé dans accu à la pile
						mz.stack.push(mz.accu);
			// les n-1 arguments sont reempiles
						for (int i = 0; i < this.n; i++)
							mz.stack.push(stack.pop());						
			//mettre la valeur () dans accu
						bloc b =new bloc();
						mz.accu=b;
			
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
		mz.pc++;

	}

	@Override
	public String toString() {
		return "ASSIGN " + this.n;
	}

}
