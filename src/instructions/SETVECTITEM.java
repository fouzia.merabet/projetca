package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.*;

public class SETVECTITEM implements Instruction {
	public SETVECTITEM() {};

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {

			//dépiler n du stack
			Int n = (Int) mz.stack.pop().clone();
			//dépiler v du stack
			MLvalue v = mz.stack.pop().clone();
			// mettre v dans la n-iéme valeur du bloc situé dans accu
			
			
			((bloc)mz.stack.get(mz.stack.size()-1)).set(n.getValue(), v);
			
			//mz.stack.push(((bloc)mz.accu));
			//mettre la valeur () dans accu
			bloc b =new bloc();
			mz.accu=b;
			//mz.stack.push(v);
			//mz.stack.push(n);
			
			} catch (Exception e) {
				throw new InstructionEvaluationException(this.toString());
			}
		mz.pc++;

		
	}
	@Override
	public String toString() {
		return "SETVECTITEM" ;
	}

}
