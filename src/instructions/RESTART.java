package instructions;

import miniZAM.*;
import tools.*;

public class RESTART implements Instruction {

	public RESTART() {

	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {

		try {
			int n = mz.env.size();

			for (int i = 1; i < n; i++)
				mz.stack.push(mz.env.get(i).clone());

			mz.env = (Env) mz.env.get(0).clone();
			mz.extra_args += n - 1;

			mz.pc++;

		} catch (Exception e) {
			e.printStackTrace();
			throw new InstructionEvaluationException(this.toString());
		}

	}

	@Override
	public String toString() {
		return "RESTART";
	}

}
