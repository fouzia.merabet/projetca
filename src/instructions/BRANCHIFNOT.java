package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.Int;

public class BRANCHIFNOT implements Instruction {

	private String label;

	public BRANCHIFNOT(String label) {
		this.label = label;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			if (mz.accu!= null) {
				Int x=new Int(0);
				if (mz.accu.getClass().isInstance(x)) {
					int arg = ((Int) mz.accu).getValue();
					if (arg == 0)
						mz.pc = mz.address.get(this.label);
					else
						mz.pc++;
				}
				else {
					mz.pc++;
				}
				}
					
				
				mz.pc = mz.address.get(this.label);}
			
			catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "BRANCHIFNOT " + this.label;
	}

}
