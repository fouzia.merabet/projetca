package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.*;

public class VECTLENGTH implements Instruction{


	public VECTLENGTH() {
	
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
		// la taille du bloc contenu dans accu sera mise dans l'accu
			mz.accu= new Int(((bloc)mz.accu).size());
				
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
		mz.pc++;

	}
	@Override
	public String toString() {
		return "VECTLENGTH" ;
	}
}
