package instructions;

import miniZAM.MiniZAM;
import tools.Env;
import tools.InstructionEvaluationException;
import types.*;

public class RAISE implements Instruction {

	private String label;

	public RAISE() {;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			if (mz.trap_sp== null) {
			System.out.println("stop");
			mz.works = false;
			}
			else {
				int i=0;
				/*while (i<mz.stack.size()) {
					if (mz.stack.get(i)==mz.trap_sp)
					{
						mz.stack.pop();
						i++;
					}else break;
					
				}*/
				mz.pc=((Int) mz.stack.pop()).getValue();
				mz.trap_sp=((Int) mz.stack.pop());
				mz.env=((Env) mz.stack.pop());
				mz.extra_args=((Int) mz.stack.pop()).getValue();
			}
			
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "RAISE";
	
	}

}
