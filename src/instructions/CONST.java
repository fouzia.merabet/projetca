package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.Int;

public class CONST implements Instruction {

	private Int arg;

	public CONST(Int arg) {
		this.arg = arg;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			mz.accu = this.arg.clone();
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "CONST " + this.arg;
	}

}
