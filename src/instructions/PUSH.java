package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;

public class PUSH implements Instruction {

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			mz.stack.push(mz.accu.clone());
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "PUSH";
	}

}
