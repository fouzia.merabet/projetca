package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.bloc;

public class GETFIELD implements Instruction {
	
	private int n;

	public GETFIELD(int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
		// la n-éme valeur du bloc contenu dans accu sera mise dans l'accu
			mz.accu= ((bloc)mz.accu).get(this.n);
				
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

		mz.pc++;

	}
	@Override
	public String toString() {
		return "GETFIELD" + this.n;
	}
}
