package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class APPLY implements Instruction {

	private int n;

	public APPLY(int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {

			// n arguments sont depiles
			Stack stack = new Stack();
			for (int i = 0; i < this.n; i++)
				stack.push(mz.stack.pop());

			// env puis pc+1 sont empiles
			mz.stack.push(mz.env.clone());
			mz.stack.push(new Int(mz.pc + 1));
			mz.stack.push(new Int(mz.extra_args));
			mz.extra_args = this.n - 1;

			// les n arguments sont reempiles
			for (int i = 0; i < this.n; i++)
				mz.stack.push(stack.pop());

			// pc recoit le pointeur de la fermeture situee dans accu
			Closure cls = (Closure) mz.accu;
			mz.pc = mz.address.get(cls.getLabel());

			// env recoit l'environnement de la fermeture situee dans accu
			mz.env = (Env) cls.getEnv().clone();

		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

	}

	@Override
	public String toString() {
		return "APPLY " + this.n;
	}

}
