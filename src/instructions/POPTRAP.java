package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.Int;

public class POPTRAP implements Instruction {
	
	public POPTRAP() {
		
	}
	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			mz.stack.pop();
			mz.trap_sp= (Int) mz.stack.pop();
			mz.stack.pop();
			mz.stack.pop();
			mz.pc++;
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

	}
	@Override
	public String toString() {
		return "POPTRAP";
	}
}
