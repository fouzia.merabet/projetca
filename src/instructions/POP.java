package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;

public class POP implements Instruction {

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
		//	if (!mz.stack.isEmpty())
				mz.stack.pop();
			mz.pc++;
		} catch (Exception e) {
			e.printStackTrace();
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "POP";
	}

}
