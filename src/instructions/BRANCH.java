package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;

public class BRANCH implements Instruction {

	private String label;

	public BRANCH(String label) {
		this.label = label;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			mz.pc = mz.address.get(this.label);
		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "BRANCH " + this.label;
	}

}
