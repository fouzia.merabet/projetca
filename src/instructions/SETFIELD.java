package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;
import types.*;

public class SETFIELD implements Instruction {
	
	private int n;

	public SETFIELD (int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			// dépiler un elt du stack
				MLvalue elt = mz.stack.pop().clone();
			// mettre elt dans la n-ième valeur du bloc situé dans accu
				((bloc)mz.accu).add(this.n, elt);
					
			} catch (Exception e) {
				throw new InstructionEvaluationException(this.toString());
			}

		mz.pc++;

	}
	@Override
	public String toString() {
		return "SETFIELD"+ this.n;
	}
}
