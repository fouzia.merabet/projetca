package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class OFFSETCLOSURE implements Instruction {
	public OFFSETCLOSURE() {
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {

		try {

			String label = mz.labels.get(((Int) mz.env.get(0)).getValue());
			Env env = (Env) mz.env.clone();

			mz.accu = new Closure(label, env);

			mz.pc++;

		} catch (Exception e) {
			throw new InstructionEvaluationException(this.toString());
		}

	}

	@Override
	public String toString() {
		return "OFFSETCLOSURE";
	}

}
