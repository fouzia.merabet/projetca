package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class RETURN implements Instruction {

	private int n;

	public RETURN(int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			// n valeurs sont depilees
			for (int i = 0; i < this.n; i++)
				mz.stack.pop();

			if (mz.extra_args == 0) {

				// Depiler extra_args car on ne s'en sert plus
				mz.stack.pop();

				// les valeurs associees a pc et env sont depilees
				mz.pc = ((Int) mz.stack.pop()).getValue();
				mz.env = (Env) mz.stack.pop().clone();
			} else {
				mz.extra_args--;
				Closure cls = (Closure) mz.accu.clone();
				mz.pc = mz.address.get(cls.getLabel());
				mz.env = cls.getEnv();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new InstructionEvaluationException(this.toString());
		}
	}

	@Override
	public String toString() {
		return "RETURN " + this.n;
	}

}
