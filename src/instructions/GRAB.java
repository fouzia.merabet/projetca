package instructions;

import miniZAM.*;
import tools.*;
import types.*;

public class GRAB implements Instruction {

	private int n;

	public GRAB(int n) {
		this.n = n;
	}

	@Override
	public void apply(MiniZAM mz) throws InstructionEvaluationException {
		try {
			if (mz.extra_args >= this.n) {
				mz.extra_args -= this.n;
				mz.pc++;
			} else {

				// Depiler extra_args+1 elements
				Stack stack = new Stack();
				for (int i = 0; i < mz.extra_args + 1; i++)
					stack.add(mz.stack.pop().clone());

				// Creer une fermeture {pc-1, env avec env[0]=pc-1 et les elements depiler
				// auparavant}
				Env env = new Env();
				env.add(mz.env.clone());
				env.addAll(stack);

				// L'acc recoit cette fermeture
				mz.accu = new Closure(mz.labels.get(mz.pc - 1), env);

				// Depilement des valeurs
				mz.extra_args = ((Int) mz.stack.pop()).getValue();
				mz.pc = ((Int) mz.stack.pop()).getValue();
				mz.env = (Env) mz.stack.pop().clone();

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new InstructionEvaluationException(this.toString());
		}

	}

	@Override
	public String toString() {
		return "GRAB " + this.n;
	}

}
