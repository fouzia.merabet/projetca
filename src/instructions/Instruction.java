package instructions;

import miniZAM.MiniZAM;
import tools.InstructionEvaluationException;

public interface Instruction {

	void apply(MiniZAM mz) throws InstructionEvaluationException;

}
