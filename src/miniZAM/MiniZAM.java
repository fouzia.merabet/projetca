package miniZAM;

import java.util.Map.Entry;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import tools.*;
import types.*;
import instructions.*;

public class MiniZAM {

	// MiniZAM state
	public Prog prog;
	public Stack stack;
	public Env env;
	public int pc;
	public MLvalue accu;
	public int extra_args;
	// ajout pour les exceptions 
	public Int trap_sp;
	
	// Other attributs
	public boolean works;
	public HashMap<String, Integer> address;
	public HashMap<Integer, String> labels;

	public MiniZAM(String sourceCode) throws InstructionFormatException {
		this.stack = new Stack();
		this.env = new Env();
		this.pc = 0;
		this.accu = new Int(0);
		this.extra_args = 0;
		this.works = true;
		this.address = new HashMap<>();
		this.labels = new HashMap<>();
		this.initProg(sourceCode);
	}

	private void initProg(String sourceCode) throws InstructionFormatException {
		try {
			Scanner scanner = new Scanner(new File(sourceCode));
			this.prog = new Prog();
			while (scanner.hasNextLine()) {
				this.prog.add(this.parseLine(scanner.nextLine()));
				this.codeOptimization();
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			throw new InstructionFormatException("(FileName = " + sourceCode + ", " + e.getMessage() + ")");
		}
	}

	private void codeOptimization() {
		int t = this.prog.size();
		if (t > 1) {
			Instruction inst1 = this.prog.get(t - 1).getElem2();
			Instruction inst2 = this.prog.get(t - 2).getElem2();
			if ((inst1 instanceof RETURN) && (inst2 instanceof APPLY)) {
				int n = ((RETURN) inst1).getN();
				int m = ((APPLY) inst2).getN();
				this.prog.remove(t - 1);
				this.prog.remove(t - 2);
				this.prog.add(new Pair<>("", (Instruction) new APPTERM(n, m)));
			}
		}

	}

	private Pair<String, Instruction> parseLine(String line) throws InstructionFormatException {
		String label = "";
		int numLine = this.prog.size();
		Instruction inst = null;
		try {
			String[] cps = line.split("\t");
			if (cps[0].length() != 0) { // Existe un label
				cps[0] = cps[0].substring(0, cps[0].length() - 1);
				label = cps[0];
				this.address.put(label, numLine);
				this.labels.put(numLine, label);
			}
			cps = cps[1].split(" |,");
			switch (cps[0]) {
			case "PUSH":
				inst = new PUSH();
				break;
			case "POP":
				inst = new POP();
				break;
			case "STOP":
				inst = new STOP();
				break;
			case "CONST":
				inst = new CONST(new Int(Integer.parseInt(cps[1])));
				break;
			case "PRIM":
				inst = new PRIM(cps[1]);
				break;
			case "BRANCH":
				inst = new BRANCH(cps[1]);
				break;
			case "BRANCHIFNOT":
				inst = new BRANCHIFNOT(cps[1]);
				break;
			case "ACC":
				inst = new ACC(Integer.parseInt(cps[1]));
				break;
			case "ENVACC":
				inst = new ENVACC(Integer.parseInt(cps[1]));
				break;
			case "APPLY":
				inst = new APPLY(Integer.parseInt(cps[1]));
				break;
			case "RETURN":
				inst = new RETURN(Integer.parseInt(cps[1]));
				break;
			case "CLOSURE":
				inst = new CLOSURE(cps[1], Integer.parseInt(cps[2]));
				break;
			case "CLOSUREREC":
				inst = new CLOSUREREC(cps[1], Integer.parseInt(cps[2]));
				break;
			case "OFFSETCLOSURE":
				inst = new OFFSETCLOSURE();
				break;
			case "GRAB":
				inst = new GRAB(Integer.parseInt(cps[1]));
				break;
			case "RESTART":
				inst = new RESTART();
				break;
			case "APPTERM":
				inst = new APPTERM(Integer.parseInt(cps[1]), Integer.parseInt(cps[2]));
				break;
			case "MAKEBLOCK":
				inst = new MAKEBLOCK(Integer.parseInt(cps[1]));
				break;
			case "GETFIELD":
				inst = new GETFIELD(Integer.parseInt(cps[1]));
				break;
			case "VECTLENGTH":
				inst = new VECTLENGTH();
				break;
			case "GETVECTITEM":
				inst = new  GETVECTITEM();
				break;
			case "SETFIELD":
				inst = new SETFIELD(Integer.parseInt(cps[1]));
				break;
			case "SETVECTITEM":
				inst = new  SETVECTITEM();
				break;
			case "ASSIGN":
				inst = new ASSIGN (Integer.parseInt(cps[1]));
				break;
			case "POPTRAP":
				inst = new POPTRAP();
				break;
			case "PUSHTRAP":
				inst = new PUSHTRAPL (cps[1]);
				break;
			case "RAISE":
				inst = new RAISE();
				break;
			
			default:
				throw new Exception();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new InstructionFormatException("Line = " + (numLine + 1) + ", Content = " + line);
		}
		return new Pair<>(label, inst);
	}

	public MLvalue run() throws InstructionEvaluationException {
		try {
			System.out.println("BEGIN (" + this + ")\n");
			while (true) {
				Instruction inst = this.prog.get(this.pc).getElem2();
				inst.apply(this);
				System.out.print(inst);
				if (!this.works) {
					System.out.println("\n\n>>>(RESULT = " + this.accu + ")");
					break;
				}
				System.out.println(" (" + this + ")\n");
			}

			return this.accu;
		} catch (InstructionEvaluationException e) {
			throw new InstructionEvaluationException("Line = " + this.pc + ", Instruction = " + e.getMessage());
		}
	}

	public void showProg() {
		System.out.println("Source Code :");
		for (Pair<String, Instruction> ligne : this.prog)
			System.out.println(ligne.getElem1() + "\t" + ligne.getElem2());
		System.out.println("\nLabel=Address :");
		for (Entry<String, Integer> entry : this.address.entrySet()) {
			System.out.println(entry);
		}
	}

	@Override
	public String toString() {
		return "pc=" + this.pc + "; accu=" + this.accu + "; stack=" + this.stack + "; env=" + this.env + "; extra_args="
				+ extra_args;
	}

}
