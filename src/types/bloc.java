package types;

import java.util.ArrayList;

public class bloc extends ArrayList<MLvalue> implements MLvalue {
   
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8976125378574744771L;
	
	public bloc () {
		super();
	}

	@Override
	public MLvalue clone() {
		bloc copy_bloc = new bloc();
		for (MLvalue elem : this)
			copy_bloc.add(elem.clone());
		return copy_bloc;
	}
	@Override
	public String toString() {
		String str = "(";
		if (!this.isEmpty()) {
			str += this.get(0);
			for (int i = 1; i < this.size(); i++)
				str += "," + this.get(i);
		}
		return str + ")";
	}

}
