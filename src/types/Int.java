package types;

public class Int implements MLvalue {

	private int value;

	public Int(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return this.value + "";
	}

	@Override
	public Int clone() {
		return new Int(this.value);
	}

}
