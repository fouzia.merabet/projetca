package types;

import tools.*;

public class Closure implements MLvalue {

	private String label;
	private Env env;

	public Closure(String label, Env env) {
		this.label = label;
		this.env = env;
	}

	public String getLabel() {
		return this.label;
	}

	public Env getEnv() {
		return env;
	}

	@Override
	public String toString() {
		return "{" + this.label + "," + this.env + "}";
	}

	@Override
	public MLvalue clone() {
		return new Closure(this.label, (Env) this.env.clone());
	}

}
