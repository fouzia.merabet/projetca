package tools;

import java.util.ArrayList;
import types.MLvalue;

public class Stack extends ArrayList<MLvalue> {

	private static final long serialVersionUID = 1L;

	public Stack() {
		super();
	}

	public void push(MLvalue element) {
		this.add(0, element);
	}

	public MLvalue pop() {
		MLvalue element = this.get(0);
		this.remove(0);
		return element;
	}

	@Override
	public String toString() {
		String str = "[";
		if (!this.isEmpty()) {
			str += this.get(0);
			for (int i = 1; i < this.size(); i++)
				str += "," + this.get(i);
		}
		return str + "]";
	}
}
