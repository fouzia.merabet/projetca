package tools;

public class InstructionEvaluationException extends Exception {

	private static final long serialVersionUID = 1L;

	public InstructionEvaluationException(String instruction) {
		super(instruction);
	}

}
