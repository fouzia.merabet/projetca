package tools;

public class InstructionFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public InstructionFormatException() {
		super();
	}

	public InstructionFormatException(String e) {
		super(e);
	}

}
