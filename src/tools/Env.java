package tools;

import java.util.ArrayList;

import types.MLvalue;

public class Env extends ArrayList<MLvalue> implements MLvalue {

	private static final long serialVersionUID = 1L;

	public Env() {
		super();
	}

	@Override
	public MLvalue clone() {
		Env copy_env = new Env();
		for (MLvalue elem : this)
			copy_env.add(elem.clone());
		return copy_env;
	}

	@Override
	public String toString() {
		String str = "<";
		if (!this.isEmpty()) {
			str += this.get(0);
			for (int i = 1; i < this.size(); i++)
				str += "," + this.get(i);
		}
		return str + ">";
	}

}
